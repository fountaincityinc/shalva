<?php



/**



 * Template Name: Front page



 *



 * @package WordPress



 * @subpackage Twenty_Fourteen



 * @since Twenty Fourteen 1.0



 */







get_header();







?>





 <?php if( get_field('slider_checkbox') ){ ?> 	 

	 

	 

     

      <!--  / banner container \ -->



     <section id="bannerCntr" class="sliderCntr">



     



     	<section class="centering">



     

     

     	<div class="flexslider loading" > 

        

       

       <?php if( have_rows('home_slider') ): ?>



	<ul class="slides">



	<?php while( have_rows('home_slider') ): the_row(); 

	

		$image = get_sub_field('slider_image');

		
		
		

		?>



		<li>





 <div class="banner_image" style=" background: url('<?php echo $image; ?>') no-repeat center center; background-size: cover"> </div>

        </li>       

        

        <?php endwhile; ?>



	</ul>



<?php endif; wp_reset_query(); ?>






             </div>	


 			
            
            <div class="banner_text">  

                
  <?php if(have_posts()): while(have_posts()) : the_post();  ?> 
  
  	
    		<?php the_content(); ?>
    
  
  
    
    <?php endwhile; endif; wp_reset_query(); ?>
  


                

               </div> 









     



     	</section>



     



     



     </section>



     <!--  \ banner container / -->

     

      <?php

		

		 }

		 

		 else {

			 

			 ?>

			 



 <!--  / banner container \ -->

     <section id="bannerCntr" class="homeCntr">



     



     	<section class="centering">



     

     

     	<div class="flexslider" > 

        

        

        	<ul class="slides">

            

            	

                <?php if(have_posts()): while(have_posts()) : the_post();  ?> 

                

                

                <li>  <?php   $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>



    <div class="banner_image" style=" background: url('<?php echo $url; ?>') no-repeat center center; background-size: cover"> </div>

               

        

        

               

               

               </li>

                

                

                

                

                 <?php endwhile;endif; wp_reset_query(); ?>

            

            

            

            

            

            </ul>

            

            

             </div>	
             
             
              <div class="banner_text">  

                
   <?php if(have_posts()): while(have_posts()) : the_post();  ?> 
                

                <?php the_content(); ?>

                
 <?php endwhile;endif; wp_reset_query(); ?>
                

                

               </div> 








     



     	</section>



     



     



     </section>

<!--  \ banner container / -->

			 

			 

			 <?php

			 }



	 ?> 







 	<section class="centering">



    <!-- / button container  \ -->

          

   <section id="buttonCntr"> 

   

 

 

 	

    

    <!-- / button box \-->

    <div class="buttonBox">

    

    

    

    	  <?php if( have_rows('button') ): ?>



	<ul>



	<?php while( have_rows('button') ): the_row(); 

	

		$btntext = get_sub_field('button_text');

		$btnurl = get_sub_field('button_url');

	

	

	if($btnurl) { 

		?>



		



		<li class="button">

		

        

        

        <a href="<?php echo $btnurl; ?> "> <?php echo $btntext; ?> </a>

        









		</li>



	<?php }  endwhile; ?>



	</ul>



<?php endif; ?>

    

    

    

    

    

    

    </div>

     <!-- \ button box /-->

     

     

     

     <article class="btn_left_sec">

     

     

      <?php the_field('left_section_text'); ?> 

     

     

      </article> 

      

      

      

      <aside class="btn_right_sec">

      

      

       <?php dynamic_sidebar('recentpost_sidebar'); ?> 

      

      

      

       </aside> 

       

       

       

     

     

    

    

    

    

   

 

   

   

   

   

   </section>     

         

 <!-- \ button container  / -->

  </section>      













<?php get_footer(); ?>



