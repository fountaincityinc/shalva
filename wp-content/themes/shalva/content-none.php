<?php


/**


 * The template for displaying a "No posts found" message.


 *


 * @package WordPress


 * @subpackage SSX_THEME


 * @since SSXTHEME 1.0


 */


?>





     <!--  / left container \ -->


    <div  class="bottomCntr leftCntr">


    
    

<div class="page-content">


	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>





	<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'ssxtheme' ), admin_url( 'post-new.php' ) ); ?></p>





	<?php elseif ( is_search() ) : ?>





	<h4><?php _e( 'Sorry, nothing matched. Please try again with different keyword.', 'ssxtheme' ); ?></h4>





	<?php else : ?>





	<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'ssxtheme' ); ?></p>


	<?php get_search_form(); ?>





	<?php endif; ?>


</div><!-- .page-content -->








</div>


