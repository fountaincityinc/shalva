<?php




get_header();



?>






<section class="centering">
<!-- / content container  \--> 
<section id="contentCntr">

<div id="leftCntr">

 <?php 
 $page_for_posts = get_option('page_for_posts');
 
  $postpage_imageid = wp_get_attachment_image_src(get_post_thumbnail_id($page_for_posts),'allfeatureimg');
  

 if($postpage_imageid){
 
  ?>
  
  <img src="<?php echo $postpage_imageid[0]; ?>" alt="" />
  
  
 <?php } ?>
 

<div class="titleBox">

<?php dynamic_sidebar('leftmenu'); ?>



</div>	



</div>



	  <!-- / right container  \-->
  <section id="rightCntr" class=" blog">
    
    
    <section class="postBox">
    
    
     <?php if(have_posts()):while(have_posts()):the_post();
	 
	 
	   $file_upload = get_field('file_upload',get_the_ID());
	  ?> 
    
   
   	<div class="postloop">
    
    <?php if($file_upload){ ?>
    
    <h4 class="title"><a href="<?php echo $file_upload; ?> " target="_blank"><?php the_title(); ?></a>  </h4>
    
    
    <?php } else {?>
    
    
    	<h4 class="title"><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a>  </h4>
        
        <?php } ?>
        
      <!--  <div class="meta">
        
        <span><?php //the_time('M j, Y') ?> </span>
        
        
        </div> -->
        
        
        
   
   
      
      
      </div>
      <?php endwhile; 
	  
 if(function_exists('wp_paginate')) {
    wp_paginate();
}
	  endif; wp_reset_query(); ?> 
      
      
      </section>
    
    
    
    
    </section>
     <!-- \ right container  /-->
     
 
 

 </section>
 
 <!-- \ content container  /--> 
   
     </section>
 
<?php get_footer(); ?>

