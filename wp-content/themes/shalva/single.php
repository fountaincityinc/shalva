<?php




get_header();



?>

<section class="centering"> 
  <!-- / content container  \-->
  <section id="contentCntr"> 
    
    <!-- / right container  \-->
    <section id="rightCntr" class="fullwidth blog">
      <section class="postBox">
        <?php if(have_posts()):while(have_posts()):the_post(); 
		
		 $file_upload = get_field('file_upload',get_the_ID());
		
		?>
        <div class="postloop">
         
            
             <?php if($file_upload){ ?>
    
    <h3 class="singletitle"><a href="<?php echo $file_upload; ?> " target="_blank"><?php the_title(); ?></a>  </h4>
    
    
    <?php } else {?>
    
    
    	<h3 class="singletitle"><?php the_title(); ?></h3>
        
        <?php } ?>
        
        
        	<?php the_content(); ?> 
        
        </div>
        <?php endwhile; endif; wp_reset_query(); ?>
        
       
	      
          <div class="singlepostnav">
          
          
   <?php previous_post_link( '%link', '<div class="prev">Previous </div>', false ); ?>
    <?php next_post_link( '%link', '<div class="next">Next</div>', false ); ?>

        
        </div>
        
        
      </section>
    </section>
    <!-- \ right container  /--> 
    
  </section>
  
  <!-- \ content container  /--> 
  
</section>
<?php get_footer(); ?>
