<?php 
 session_start();
?><!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mmenu.all.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/font/font.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox.css">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon1.png" />
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!--[if IE]><script  type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script><![endif]-->
<!-- jQuery -->
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
<script  type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.cookie.js"></script>
<!--[if IE]><script  type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
</head>
<body <?php body_class(); ?>>
<!-- site !-->
<section id="site">
<!--  / wrapper \ -->
<section id="wrapper">
 
 
 
 
<?php
 /*
 if(is_front_page() && $_SESSION['varPop']!=1) {  ?>
 <div id="alertCntr">
 
 
 		<?php dynamic_sidebar('popup_sidebar'); ?>
    
  </div>
  
  <?php 
		 
 $_SESSION['varPop']=1;
		 } */
 ?>
	<!--  / header container \ -->
		<header id="headerCntr">
         <a class="mobilemenu" href="#menu"><span></span>Menu</a><a href="#" class="close"><span>Close</span></a>
        
       		<!-- / menu Box \ -->     
			<div class="menuBox" > 
			<section class="centering">
            
               <nav id="menu">
               
               
	<?php wp_nav_menu(array('theme_location'=>'primary-menu')); ?>
             </nav>
		</section>
        
        <div class="clear"></div>
             </div>
                
             
             
            <!-- \ menu Box / -->
            
            
            
            
            <div class="logoBox">
            
            
       			<section class="centering" > 
               
					 <a href="<?php bloginfo('url'); ?>" class="logo"> <img src="<?php if ( the_field('logo', 'option') ) { echo the_field('logo', 'option');} ?>" alt="<?php bloginfo('title'); ?>" /> </a>
            		
				
                	<aside class="call_sec">
							 <?php dynamic_sidebar('headertop_sidebar'); ?> 		
                        
                        
                     </aside>   
                    
                    
	     	
        
   </section>
            
 </div> 
       
            
            
        
        
            
         <div class="clear" > </div>
        
         </header>
     <!--  \ header container / -->    
     
     
    