<?php



get_header(); ?>


<section class="centering">
<!-- / content container  \--> 
<section id="contentCntr">



	  <!-- / right container  \-->
  <section id="rightCntr" class="fullwidth blog">
    
    
    <section class="postBox">
   
 
    <h1 class="pagetitle"><?php printf( __( 'Search for: %s', 'ssxtheme' ), get_search_query() ); ?></h1>
   
   
   
   <?php if(have_posts()):while(have_posts()):the_post(); 
	 
	  $file_upload = get_field('file_upload',get_the_ID());
	  ?> 
    
   
   	<div class="postloop">
    
    <?php if($file_upload){ ?>
    
    <h4 class="title"><a href="<?php echo $file_upload; ?> " target="_blank"><?php the_title(); ?></a>  </h4>
    
    
    <?php } else {?>
    
    
    	<h4 class="title"><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a>  </h4>
        
        <?php } ?>
        
         <!--  <div class="meta">
        
        <span><?php //the_time('M j, Y') ?> </span>
        
        
        </div> -->
       </div>
      <?php endwhile;  else: ?> 
 
					 <h4>Nothing found</h4>
                    <p> Sorry, but nothing matched your search terms. Please try again with different keywords. </p>
                   



      <?php endif; ?>
      
       
       

   
   
    
     </section>
    
    
    
    
    </section>
     <!-- \ right container  /-->
     
 
 

 </section>
 
 <!-- \ content container  /--> 
   
     </section>
 
<?php get_footer(); ?>



     
   
   

 
 